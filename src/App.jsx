import { useRef, useState } from "react";
import "./App.css";

const App = () => {
  const ref = useRef();
  const [value, setValue] = useState("");

  const onKeyPress = (e) => {
    if (
      (e.code === "KeyC" || e.code === "KeyX") &&
      e.ctrlKey &&
      ref.current.selectionStart === ref.current.selectionEnd
    ) {
      const allLines = value.split("\n");
      const allLinesLengths = allLines.map((line) => line.length);
      const getLengthsTillIdx = (idx) =>
        allLinesLengths.slice(0, idx).reduce((a, b) => a + b, 0);
      let textToCopy = "";
      let idxToCutCopy = 0;
      if (ref.current.selectionStart === 0) {
        textToCopy = allLines[0];
      } else {
        let isGreater = false;
        allLines.forEach((_, idx) => {
          console.log({
            idx,
            len: allLinesLengths[idx],
            lenTill: getLengthsTillIdx(idx + 1),
            sel: ref.current.selectionStart,
          });
          if (
            getLengthsTillIdx(idx + 1) >= ref.current.selectionStart - idx &&
            !isGreater
          ) {
            console.log(idx);
            isGreater = true;
            idxToCutCopy = idx;
            textToCopy = allLines[idxToCutCopy];
          }
        });
      }

      navigator.clipboard
        .writeText(textToCopy)
        .then(() => {
          if (e.code === "KeyX") {
            const allLinesCopy = [...allLines];
            allLinesCopy.splice(idxToCutCopy, 1), console.log(allLinesCopy);
            setValue(allLinesCopy.join("\n"));
          }
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <div className="App">
      <textarea
        ref={ref}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onKeyUp={onKeyPress}
      />
    </div>
  );
};

export default App;
